
package com.chromaway.aiponi;

import net.postchain.gtx.GTXDataBuilder;
import net.postchain.gtx.GTXValue;
import net.postchain.test.IntegrationTest;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;

import static net.postchain.gtx.ValuesKt.gtx;

public class ETKIntegrationTest extends IntegrationTest {
    byte[] issuerPubKey = bytes("03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8");
    byte[] issuerPrivKey = bytes("3132333435363738393031323334353637383930313233343536373839303133");
    byte[] blockchainRID = bytes("5d0502f38c4610e702ee85754f2e5a94c1907c58fc793f3920b2b663ce623fe3");

    private byte[] makeIssueTx(byte[] toAddr, Long amount) {
        byte[][] signers = {issuerPubKey};
        GTXDataBuilder b = new GTXDataBuilder(blockchainRID, signers, getCryptoSystem());
        GTXValue[] gtxValues = {gtx(toAddr), gtx(amount)};
        b.addOperation("etk_issue", gtxValues);
        b.finish();
        b.sign(getCryptoSystem().makeSigner(issuerPubKey, issuerPrivKey));
        return b.serialize();
    }

    private byte[] makeTransferTx(byte[] fromPriv, byte[] fromAddr, byte[] toAddr, Long amount) {
        byte[][] signers = {fromAddr};
        GTXDataBuilder b = new GTXDataBuilder(blockchainRID, signers, getCryptoSystem());
        GTXValue[] gtxValues = {gtx(fromAddr), gtx(toAddr), gtx(amount)};
        b.addOperation("etk_transfer", gtxValues);
        b.finish();
        b.sign(getCryptoSystem().makeSigner(fromAddr, fromPriv));
        return b.serialize();
    }

    private byte[] setRock(byte[] fromPriv, byte[] fromAdd, String slogan, Long record_date) {
        byte[][] signers = {fromAdd};
        GTXDataBuilder b = new GTXDataBuilder(blockchainRID, signers, getCryptoSystem());
        GTXValue[] gtxValues = {gtx(slogan),gtx(record_date)};
        b.addOperation("set_slogan", gtxValues);
        b.finish();
        b.sign(getCryptoSystem().makeSigner(fromAdd, fromPriv));
        return b.serialize();
    }

    @Test
    public void doTest() throws Exception {

        getConfigOverrides().setProperty("blockchain.1.gtx.modules", ModuleFactory.class.getName());
        getConfigOverrides().setProperty("blockchain.1.gtx.etk.issuerPubKey", hex(issuerPubKey));
        getConfigOverrides().setProperty("blockchain.1.blockchainrid", hex(blockchainRID));
        getConfigOverrides().setProperty("database.schema", "test_etk");


        long[] currentBlockHeight = {-1L};

        byte[] alicePub = pubKey(1);
        byte[] bobPub = pubKey(2);
        byte[] alicePriv = privKey(1);
        byte[] bobPriv = privKey(2);


    }


    private byte[] bytes(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }

    private String hex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes).toLowerCase();
    }

}