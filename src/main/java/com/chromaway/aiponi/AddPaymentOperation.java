package com.chromaway.aiponi;

import net.postchain.core.TxEContext;
import net.postchain.core.UserMistake;
import net.postchain.gtx.ExtOpData;
import net.postchain.gtx.GTXOperation;
import net.postchain.gtx.GTXValue;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;

public class AddPaymentOperation extends GTXOperation {

    private String identification;
    private final long record_date;
    private final long payment;
    private final long amount;

    AddPaymentOperation(ExtOpData opData) {
        super(opData);
        GTXValue[] args = opData.getArgs();
        payment = args[0].asInteger();
        record_date = args[1].asInteger();
        identification = args[2].asString();
        amount = args[3].asInteger();
    }

    @Override
    public boolean isCorrect() {
        // add more checks when specs are clearer
        return amount > 0;
    }

    @Override
    public boolean apply(TxEContext ctx) {
        QueryRunner r = new QueryRunner();
        ScalarHandler<String> voidHandler = new ScalarHandler<String>();
        try {
            r.query(ctx.getConn(), "SELECT add_payment(?,?,?,?)",
                    voidHandler,
                    identification,
                    record_date,
                    payment,
                    amount
            );
        } catch (SQLException e) {
            throw new UserMistake("Error in transfer", e);
        }
        return true;
    }
}
